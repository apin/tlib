package tlib

import (
    "crypto/md5"
    "encoding/hex"
    "encoding/json"
)

func GetMd5(val string) string {
    hasher := md5.New()
    hasher.Write([]byte(val))
    return hex.EncodeToString(hasher.Sum(nil))
}

func StructToMap(val interface{}) map[string]interface{} {
    m := make(map[string]interface{})
    d, _ := json.Marshal(val)
    json.Unmarshal(d, &m)
    return m
}