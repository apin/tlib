package tlib

import (
    "github.com/jinzhu/gorm"
    "github.com/jinzhu/now"
    "net/url"
    "strconv"
    "strings"
    "time"
)

func HandleQuery(data interface{}, db *gorm.DB) *gorm.DB {
    var query url.Values
    if data != nil {
        query = data.(url.Values)
    } else {
        return db
    }
    //handle limit
    if val, ok := query["limit"]; ok {
        if len(val) > 0 {
            if limit, err := strconv.Atoi(val[0]); err == nil {
                db = db.Limit(limit)
            }
        }
    }
    //handle offset
    if val, ok := query["offset"]; ok {
        if len(val) > 0 {
            if offset, err := strconv.Atoi(val[0]); err == nil {
                db = db.Offset(offset)
            }
        }
    }
    //orders
    if val, ok := query["order"]; ok {
        if len(val) > 0 {
            ord := strings.Replace(val[0], "$", " ", 1)
            db = db.Order(ord)
        }
    }
    return db
}

func HandleDelete(q url.Values, db *gorm.DB, deleted bool, key string) *gorm.DB {
    deletestr := "deleted_at"
    if key != "" {
        deletestr = key + "." + deletestr
    }
    /*if dealer {
        ret = ret.Where(dealerstr + " = ?", sess.Token.DealerId)
    }*/
    if deleted {
        if q.Get("deleted") == "only" {
            db = db.Where(deletestr + " IS NOT NULL")
        } else
        if q.Get("deleted") == "" {
            db = db.Where(deletestr + " IS NULL")
        }
    }
    return db
}

func HandleDateStartEnd(q url.Values, db *gorm.DB, key string, strOr *string) *gorm.DB {
    var ns = now.New(time.Now()).BeginningOfMonth()
    var ne = now.New(time.Now()).EndOfMonth()
    if q.Get("ds") != "" && q.Get("de") != "" {
        ds, es := strconv.ParseInt(q.Get("ds"), 10, 64)
        de, ee := strconv.ParseInt(q.Get("de"), 10, 64)
        if es == nil && ee == nil {
            ns = now.New(time.Unix(ds, 0)).BeginningOfDay()
            ne = now.New(time.Unix(de, 0)).EndOfDay()
        }
        if strOr != nil {
            return db.Where("(" + key + " >= ? AND " + key + " <= ? ) OR " + *strOr, time.Time(ns), time.Time(ne))
        }
        return db.Where(key + " >= ? AND " + key + " <= ?", time.Time(ns), time.Time(ne))
    }
    return db
}

func HandleWhere(db *gorm.DB, where []interface{}) *gorm.DB {
    for _, v := range where {
        json := v.(map[string]interface{})
        k := json["query"].(string)
        key := json["key"].(string)
        switch k {
        case "in":
            db = db.Where(key + " IN (?)", json["value"])
        case "eq":
            db = db.Where(key + " = ?", json["value"])
        case "ne":
            db = db.Where(key + " != ?", json["value"])
        case "lt":
            db = db.Where(key + " < ?", json["value"])
        case "gt":
            db = db.Where(key + " > ?", json["value"])
        }
    }
    return db
}