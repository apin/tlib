package tlib

import (
    "github.com/jinzhu/gorm"
    "net/url"
    "strings"
    "strconv"
    "github.com/apinprastya/revel"
)

type WebixFilterSort struct {
    query       url.Values
    Sort        string
    Filter      map[string]string
    Start       int
    Count       int
}

var DefaultWebixLimit = 50

func (this *WebixFilterSort) Query (db *gorm.DB, deleted bool, key string) *gorm.DB {
    ret := HandleDelete(this.query, db, deleted, key)
    //ret = ret.Limit(this.Count).Offset(this.Start)
    if this.Sort != "" {
        ret = ret.Order(this.Sort)
    }
    if len(this.Filter) > 0 {
        //Where is not handled because need to know the column data type
    }
    return ret;
}

func (this *WebixFilterSort) Where (db *gorm.DB, where ...WhereQuery) *gorm.DB {
    ret := db
    for _, w := range where {
        if v := this.Filter[w.Key()]; v != "" {
            if i, err := strconv.ParseInt(v, 10, 64); err == nil {
                ret = w.Query(ret, i)
            } else {
                ret = w.Query(ret, v)
            }
        }
    }
    return ret
}

func ParseWebixFilterAndSort(data url.Values, key string) *WebixFilterSort {
    f := &WebixFilterSort{Start: 0, Count: DefaultWebixLimit}
    f.query = data
    for k, v := range data {
        if strings.HasPrefix(k, "sort") {
            f.Sort = getValue(k) + " " + v[0];
        } else
        if strings.HasPrefix(k, "filter") {
            if f.Filter == nil {
                f.Filter = make(map[string]string)
            }
            f.Filter[getValue(k)] = v[0]
        }
    }
    //handle limit
    if val, ok := data["count"]; ok {
        if len(val) > 0 {
            if limit, err := strconv.Atoi(val[0]); err == nil {
                f.Count = limit
            }
        }
    }
    //handle offset
    if val, ok := data["start"]; ok {
        if len(val) > 0 {
            if offset, err := strconv.Atoi(val[0]); err == nil {
                f.Start = offset
            }
        }
    }
    return f
}

func getValue(s string) string {
    return s[strings.Index(s, "[") + 1:len(s) - 1]
}

func ParseWebix(urlquery url.Values, db *gorm.DB, deleted bool, key string) (wq *WebixFilterSort, dbret *gorm.DB) {
    wq = ParseWebixFilterAndSort(urlquery, key)
    dbret = wq.Query(db, deleted, key)
    return
}

func WebixDynDatatableUnfilter(q url.Values, retmap revel.JsonRawMap, db *gorm.DB) {
    if q.Get("continue") == "" {
        c := 0
        db.Count(&c)
        retmap["unfilter"] = c
    }
    if q.Get("start") == "" {
        retmap["pos"] = 0
    } else {
        p, _ := strconv.ParseInt(q.Get("start"), 10, 32)
        retmap["pos"] = p
    }
}

func WebixDynDatatableFilter(q url.Values, retmap revel.JsonRawMap, db *gorm.DB) {
    c := 0
    db.Count(&c)
    retmap["total_count"] = c
}