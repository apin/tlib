package tlib

import (
    "time"
    "strconv"
    "database/sql/driver"
    "strings"
    "errors"
    "fmt"
    "regexp"
)

type Model struct {
    ID        int		    `json:"id"`
    CreatedAt time.Time		`json:"created_at"`
    UpdatedAt time.Time		`json:"updated_at"`
    DeletedAt *time.Time 	`json:"-"`
}

type ModelCreate struct {
    ID        int		    `json:"id"`
    CreatedAt time.Time		`json:"created_at"`
}

type ModelNoUpdate struct {
    ID        int		      `json:"id"`
    CreatedAt time.Time		  `json:"created_at"`
    DeletedAt *time.Time      `json:"deleted_at"`
}

type ModelNoDelete struct {
    ID        int		    `json:"id"`
    CreatedAt time.Time		`json:"created_at"`
    UpdatedAt time.Time		`json:"updated_at"`
}

type ModelHistory struct {
    ModelCreate
    UserId      string          `json:"user_id"`
    Data        string          `json:"data"`
}

type ModelDeleteOnly struct {
    ID        int		    `json:"id"`
    DeletedAt *time.Time      `json:"deleted_at"`
}

type TError struct {
    Code            int32
    ErrorString     string
}

type StringSlice    []string
type IntSlice       []int32

var (
// unquoted array values must not contain: (" , \ { } whitespace NULL)
// and must be at least one char
    unquotedChar  = `[^",\\{}\s(NULL)]`
    unquotedValue = fmt.Sprintf("(%s)+", unquotedChar)

// quoted array values are surrounded by double quotes, can be any
// character except " or \, which must be backslash escaped:
    quotedChar  = `[^"\\]|\\"|\\\\`
    quotedValue = fmt.Sprintf("\"(%s)*\"", quotedChar)

// an array value may be either quoted or unquoted:
    arrayValue = fmt.Sprintf("(?P<value>(%s|%s))", unquotedValue, quotedValue)

// Array values are separated with a comma IF there is more than one value:
    arrayExp = regexp.MustCompile(fmt.Sprintf("((%s)(,)?)", arrayValue))

    valueIndex int
)

func (ae *TError) Error() string {
    return ae.ErrorString
}

func NewTError(code int32, error string) *TError {
    return &TError{code, error}
}

func strToStringSlice(array string) []string {
    results := make([]string, 0)
    matches := arrayExp.FindAllStringSubmatch(array, -1)
    for _, match := range matches {
        s := match[valueIndex]
        // the string _might_ be wrapped in quotes, so trim them:
        s = strings.Trim(s, ",")
        s = strings.Trim(s, "\"")
        results = append(results, s)
    }
    return results
}

func strToIntSlice(s string) []int32 {
    r := strings.Trim(s, "{}")
    a := make([]int32, 0, 10)
    for _, t := range strings.Split(r, ",") {
        i, _ := strconv.ParseInt(t, 10, 32)
        a = append(a, int32(i))
    }
    return a
}

func (s StringSlice) Value() (driver.Value, error) {
    str := "{"
    for i, v := range (s) {
        if i != 0 {
            str = str + ","
        }
        str = str + "\"" + v + "\""
    }
    str = str + "}"
    return str, nil
}

func (s *StringSlice) Scan(src interface{}) error {
    asBytes, ok := src.([]byte)
    if !ok {
        return error(errors.New("Scan source was not []bytes"))
    }

    asString := string(asBytes)
    parsed := strToStringSlice(asString)
    (*s) = StringSlice(parsed)

    return nil
}

func (this StringSlice) Equal(other StringSlice) bool {
    if this == nil && other != nil {
        return false
    } else if (this != nil && other == nil) {
        return false
    } else if len(this) != len(other) {
        return false
    }
    for i := 0; i < len(this); i++ {
        if this[i] != other[i] {
            return false
        }
    }
    return true;
}

func (i IntSlice) Value() (driver.Value, error) {
    str := "{"
    for i, v := range (i) {
        if i != 0 {
            str = str + ","
        }
        str = str + strconv.FormatInt(int64(v), 10)
    }
    str = str + "}"
    return str, nil
}

func (i *IntSlice) Scan(src interface{}) error {
    asBytes, ok := src.([]byte)
    if !ok {
        return errors.New("Scan source was not []bytes")
    }

    asString := string(asBytes)
    (*i) = IntSlice(strToIntSlice(asString))

    return nil
}

func (this IntSlice) Equal(other IntSlice) bool {
    if this == nil && other != nil {
        return false
    } else if (this != nil && other == nil) {
        return false
    } else if len(this) != len(other) {
        return false
    }
    for i := 0; i < len(this); i++ {
        if this[i] != other[i] {
            return false
        }
    }
    return true;
}

func (this IntSlice) Contain(val ...int32) bool {
    for _, v := range this {
        for _, v1 := range val {
            if v == v1 {
                return true
            }
        }
    }
    return false
}