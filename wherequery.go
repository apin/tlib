package tlib

import (
    "github.com/jinzhu/gorm"
    "strings"
    "strconv"
)

func getColumnOrKey(str string, i int) string {
    if strings.Contains(str, "#") {
        return strings.Split(str, "#")[i]
    }
    return str
}

func getColumnName(str string) string {
    return getColumnOrKey(str, 0)
}

func getKeyName(str string) string {
    return getColumnOrKey(str, 1)
}

type WhereQuery interface {
    Query(db *gorm.DB, value interface{}) *gorm.DB
    Column() string
    Key() string
}

type WILike struct {
    ColumnName string
}

func (this *WILike) Query(db *gorm.DB, value interface{}) *gorm.DB {
    v, ok := value.(string)
    if !ok {
        if f, ok := value.(int64); ok {
            v = strconv.FormatInt(f, 10)
        }
    }
    return db.Where(this.Column() + " ilike ?", "%"+v+"%")
}

func (this *WILike) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WILike) Key() string {
    return getKeyName(this.ColumnName)
}

type WLike struct {
    ColumnName string
}

func (this *WLike) Query(db *gorm.DB, value interface{}) *gorm.DB {
    v, ok := value.(string)
    if !ok {
        if f, ok := value.(int64); ok {
            v = strconv.FormatInt(f, 10)
        }
    }
    return db.Where(this.Column() + " like ?", "%"+v+"%")
}

func (this *WLike) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WLike) Key() string {
    return getKeyName(this.ColumnName)
}

type WEqual struct {
    ColumnName string
}

func (this *WEqual) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " = ?", value)
}

func (this *WEqual) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WEqual) Key() string {
    return getKeyName(this.ColumnName)
}

type WNotEqual struct {
    ColumnName string
}

func (this *WNotEqual) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " != ?", value)
}

func (this *WNotEqual) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WNotEqual) Key() string {
    return getKeyName(this.ColumnName)
}

type WLess struct {
ColumnName string
}


func (this *WLess) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " < ?", value)
}

func (this *WLess) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WLess) Key() string {
    return getKeyName(this.ColumnName)
}

type WLessEqual struct {
    ColumnName string
}

func (this *WLessEqual) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " <= ?", value)
}

func (this *WLessEqual) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WLessEqual) Key() string {
    return getKeyName(this.ColumnName)
}

type WGreater struct {
    ColumnName string
}

func (this *WGreater) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " > ?", value)
}

func (this *WGreater) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WGreater) Key() string {
    return getKeyName(this.ColumnName)
}

type WGreaterEqual struct {
    ColumnName string
}

func (this *WGreaterEqual) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " >= ?", value)
}

func (this *WGreaterEqual) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WGreaterEqual) Key() string {
    return getKeyName(this.ColumnName)
}

type WNull struct {
    ColumnName string
}

func (this *WNull) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " IS NULL")
}

func (this *WNull) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WNull) Key() string {
    return getKeyName(this.ColumnName)
}

type WNotNull struct {
    ColumnName string
}

func (this *WNotNull) Query(db *gorm.DB, value interface{}) *gorm.DB {
    return db.Where(this.Column() + " IS NOT NULL")
}

func (this *WNotNull) Column() string {
    return getColumnName(this.ColumnName)
}

func (this *WNotNull) Key() string {
    return getKeyName(this.ColumnName)
}